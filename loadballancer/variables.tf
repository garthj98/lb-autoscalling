variable "my_tag" {
	description = "Name to tag all elements with"
	default = "garth"
}

variable "my_cidr" {
    description = "My IP address"
    default = "86.11.200.116/32"
}

variable "vpc_id" {
    description = "VPC ID"
    default = "vpc-4bb64132"
}