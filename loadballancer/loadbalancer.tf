resource "aws_security_group" "my-security-group" {
  name = "${var.my_tag}_server_group_public"
  vpc_id  = var.vpc_id
}

resource "aws_security_group_rule" "allow_ssh_master" {
      security_group_id = aws_security_group.my-security-group.id
      type              = "ingress"
      from_port         = 22
      to_port           = 22
      protocol          = "tcp"
      cidr_blocks       = [var.my_cidr]
}
resource "aws_security_group_rule" "allow_http" {
     security_group_id = aws_security_group.my-security-group.id
     type              = "ingress"
     from_port         = 80
     to_port           = 80
     protocol          = "tcp"
     cidr_blocks       = [var.my_cidr]
}

resource "aws_security_group_rule" "outbound_master" {
       security_group_id = aws_security_group.my-security-group.id
       type              = "egress"
       from_port         = 0
       to_port           = 0
       protocol          = -1
       cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_lb_target_group" "my-target-group" {
  name     = "tf-${var.my_tag}-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

# resource "aws_lb_target_group" "my-target-group" {
#    name               = "tf-${my_tag}-lb-tg"
#    target_type        = "instance"
#    port               = 80
#    protocol           = "HTTP"
#    vpc_id             = var.vpc_id
#   #  health_check {
#   #     healthy_threshold   = var.health_check["healthy_threshold"]
#   #     interval            = var.health_check["interval"]
#   #     unhealthy_threshold = var.health_check["unhealthy_threshold"]
#   #     timeout             = var.health_check["timeout"]
#   #     path                = var.health_check["path"]
#   #     port                = var.health_check["port"]
#   # }
# }

resource "aws_launch_template" "my-template" {
  name          = "gj-tf"
  image_id      = "ami-01e1b2743ab31158b"
  instance_type = "t2.micro"
  tags = {
    Name = "${var.my_tag}-tf"
  }
}

resource "aws_autoscaling_group" "my-asg" {
  desired_capacity   = 2
  max_size           = 5
  min_size           = 2
  availability_zones = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]

  launch_template {
    id      = aws_launch_template.my-template.id
    version = "$Latest"
  }
}

### Create load balancer

resource "aws_lb" "my_loadbalancer" {
  name               = "${var.my_tag}-lb-tf"
  # availability_zones = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.my-security-group.id]
  subnets            = ["subnet-953a58cf", "subnet-7bddfc1d"]
#   subnets            = [for subnet in aws_subnet.public : subnet.id]

#   enable_deletion_protection = true

#   access_logs {
#     bucket  = aws_s3_bucket.lb_logs.bucket
#     prefix  = "test-lb"
#     enabled = true
#   }

  tags = {
    Name = "${var.my_tag}-lb-tf"
  }
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.my_loadbalancer.arn
  # port              = "443"
  # protocol          = "HTTPS"
  # ssl_policy        = "ELBSecurityPolicy-2016-08"
  # certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my-target-group.arn
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = aws_autoscaling_group.my-asg.id
  lb_target_group_arn    = aws_lb.my_loadbalancer.arn
}