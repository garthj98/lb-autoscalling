provider "aws" {
    shared_credentials_files = ["/Users/garthjackson/.aws/credentials"]
    profile = "Academy"
    region = "eu-west-1"
}