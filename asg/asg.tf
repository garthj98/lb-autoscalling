resource "aws_launch_template" "my-template" {
#   name_prefix   = "foobar"
  image_id      = "ami-01e1b2743ab31158b"
  instance_type = "t2.micro"
}

resource "aws_autoscaling_group" "my-asg" {
  desired_capacity   = 2
  max_size           = 5
  min_size           = 2

  launch_template {
    id      = aws_launch_template.my-template.id
    version = "$Latest"
  }
}