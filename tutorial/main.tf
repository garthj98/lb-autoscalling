terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-west-1" 
}

resource "aws_lb" "main_lb" {
    name                        = "garth-lb-ch9"
    internal                    = false
    load_balancer_type          = "application"
    security_groups             = ["sg-026a52fbbe4783f73", "sg-097bc6de9c56aad49"]
    subnets                     = ["subnet-953a58cf", "subnet-7bddfc1d", "subnet-a94474e1"]
    tags                        = {
        Name                    = "garth-lb-ch9"
    }
}

resource "aws_lb_target_group" "my_tg" {
  name     = "tf-gj-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "vpc-4bb64132"
}

resource "aws_lb_listener" "tg_listener" {
  load_balancer_arn = aws_lb.main_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_tg.arn
  }
}

resource "aws_launch_template" "my-template" {
  name          = "gj-tf"
  image_id      = "ami-01e1b2743ab31158b"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["sg-026a52fbbe4783f73", "sg-097bc6de9c56aad49"]
  tags = {
    Name = "garth-tf"
  }
}

resource "aws_autoscaling_group" "my-asg" {
  desired_capacity   = 2
  max_size           = 5
  min_size           = 2
  vpc_zone_identifier = ["subnet-953a58cf", "subnet-7bddfc1d", "subnet-a94474e1"]
  launch_template {
    id      = aws_launch_template.my-template.id
    version = "$Latest"
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = aws_autoscaling_group.my-asg.id
  alb_target_group_arn    = aws_lb_target_group.my_tg.arn
}